package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"yxService/app/pkg/app"
	"yxService/app/pkg/errcode"
	"yxService/app/pkg/limiter"
)

func RateLimiter(l limiter.LimiterIface) gin.HandlerFunc {
	return func(c *gin.Context) {
		key := l.Key(c)
		if bucket, ok := l.GetBucket(key); ok {
			count := bucket.TakeAvailable(1)
			if count == 0 {
				response := app.NewResponse(c)
				response.ToErrorResponse(errors.New("访问限流拉"), errcode.TooManyRequests)
				c.Abort()
				return
			}
		}

		c.Next()
	}
}
