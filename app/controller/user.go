package controller

import (
	"github.com/gin-gonic/gin"
	"yxService/app/models"
	"yxService/app/pkg/app"
	"yxService/app/pkg/errcode"
)

type User struct {
}

func (u User) CreateUser(c *gin.Context) {
	res := app.NewResponse(c)
	user := models.User{Nickname: "ning"}

	token, err := app.GenerateToken(&user)
	if err != nil {
		res.ToErrorResponse(err, errcode.ServerError)
	}
	var resMap = make(map[string]interface{})
	resMap["token"] = token
	resMap["user"] = user
	res.ToResponse(resMap, errcode.Success)
}

func (u User) GetUser(c *gin.Context) {
	res := app.NewResponse(c)
	res.ToResponse(models.AuthUser, errcode.Success)
}
