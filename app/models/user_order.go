package models

//用户订单
type UserOrder struct {
	BaseModel
	UserId       int    `json:"user_id" gorm:"user_id"`
	Username     string `json:"username" gorm:"username"`
	Phone        string `json:"phone" gorm:"phone"`
	Province     string `json:"province" gorm:"province"`
	City         string `json:"city" gorm:"city"`
	County       string `json:"county" gorm:"county"`
	Address      string `json:"address" gorm:"address"`
	Lng          string `json:"lng" gorm:"lng"`
	Lat          string `json:"lat" gorm:"lat"`
	GoodType     string `json:"good_type" gorm:"good_type"`
	GoodTypeName string `json:"good_type_name" gorm:"good_type_name"`
	Status       string `json:"status" gorm:"status"`
}
