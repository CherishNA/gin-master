package models

//用户地址
type UserAddress struct {
	BaseModel
	UserId   int    `json:"user_id" gorm:"user_id"`
	Province string `json:"province" gorm:"province"`
	County   string `json:"county" gorm:"county"`
	City     string `json:"city" gorm:"city"`
	Address  string `json:"address" gorm:"address"`
	Lng      string `json:"lng" gorm:"lng"`//经度
	Lat      string `json:"lat" gorm:"lat"`//纬度
	Status   bool   `json:"status" gorm:"status"`
}

//todo 数据库 回收物品类型 20211102