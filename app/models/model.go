package models

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
	"yxService/app/pkg/setting"
)

var DBEngine *gorm.DB

type BaseModel struct {
	ID        int `gorm:"primary_key" json:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	//DeletedAt  gorm.DeletedAt `json:"deleted_on"`
	//IsDel      uint8          `json:"is_del"`
}

//初始化Db
func SetupDBEngine() error {
	var err error
	DBEngine, err = NewDBEngine()
	if err != nil {
		return err
	}
	return nil
}

func NewDBEngine() (*gorm.DB, error) {

	dbLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,   // 慢 SQL 阈值
			LogLevel:      logger.Silent, // Log level
			Colorful:      false,         // 禁用彩色打印
		},
	)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN: fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=%s&parseTime=%t&loc=Local",
			setting.DatabaseSetting.UserName,
			setting.DatabaseSetting.Password,
			setting.DatabaseSetting.Host,
			setting.DatabaseSetting.DBName,
			setting.DatabaseSetting.Charset,
			setting.DatabaseSetting.ParseTime,
		),
		DefaultStringSize:         256,   // string 类型字段的默认长度
		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{
		Logger: dbLogger,
	})
	if err != nil {
		return nil, err
	}

	mysqlDb, _ := db.DB()
	//// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	mysqlDb.SetMaxIdleConns(setting.DatabaseSetting.MaxIdleConns)
	// SetMaxOpenConns 设置打开数据库连接的最大数量
	mysqlDb.SetMaxOpenConns(setting.DatabaseSetting.MaxOpenConns)

	return db, nil
}
