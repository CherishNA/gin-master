package models

//垃圾类型
type RefuseType struct {
	BaseModel
	Name   string `json:"name" gorm:"name"`
	Status bool   `json:"status" gorm:"status"`
}
