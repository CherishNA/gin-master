package errcode

var (
	Success       = NewError(0, "成功")
	ServerError   = NewError(500, "服务内部错误")
	InvalidParams = NewError(400, "入参错误")
	NotFound      = NewError(404, "找不到")

	TooManyRequests = NewError(10000007, "请求过多")
	//session_id 错误
	SessionIdError = NewError(4005, "session_id is err .")
	//实体不存在
	EntityNotExist = NewError(4001, "the entity is not found")
	//Db查询错误
	DBException = NewError(4002, "DB Exception")

	/*
		401错误代码
	*/
	UnauthorizedTokenError = NewError(401, " Authorization auth failed, the token is error")
	//未提供token
	InvalidTokenParams = NewError(4011, " Authorization the auth field(:token) is required and  is not null")
	//token超时
	UnauthorizedTokenTimeout = NewError(4012, " Authorization token time out ")
	FormalAuthError          = NewError(4013, " Authorization token formal error ")

	/*
		500错误代码
	*/
	//微信授权code异常
	WechatError       = NewError(5001, "wechat exception")
	RedisError        = NewError(5002, "redis exception")
	DecryptPhoneError = NewError(5003, "decrypt phone is null,please check your encryptedData and iv")
	JWTModelError     = NewError(5004, "jwtModel reflect kind is error.")
)
