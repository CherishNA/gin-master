package setting

import (
	"github.com/silenceper/wechat/v2/cache"
	"time"
)
import miniConfig "github.com/silenceper/wechat/v2/miniprogram/config"

type ServerSettingS struct {
	RunMode      string
	HttpPort     string
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

type AppSettingS struct {
	DefaultPageSize int
	MaxPageSize     int
	LogSavePath     string
	LogFileName     string
	LogFileExt      string

	UploadSavePath       string
	UploadServerUrl      string
	UploadImageMaxSize   int
	UploadImageAllowExts []string
}

type DatabaseSettingS struct {
	DBType       string
	UserName     string
	Password     string
	Host         string
	DBName       string
	TablePrefix  string
	Charset      string
	ParseTime    bool
	MaxIdleConns int
	MaxOpenConns int
}

type JWTSettingS struct {
	Secret string
	Issuer string
	Expire time.Duration
}

//微信缓存配置
type CacheTypeSettingS struct {
	CacheType string
}

//微信配置信息
type WechatSettingS struct {
	MiniConfig      miniConfig.Config
	RedisConfig     cache.RedisOpts
	CacheTypeConfig CacheTypeSettingS
}

//Redis配置信息
type RedisSettingS struct {
	Addr     string
	Password string
	DB       int
}

//七牛云上传配置
type QiniuSettingS struct {
	AccessKey   string
	SecretKey   string
	Bucket      string
	QiniuServer string
}

var (
	DatabaseSetting = &DatabaseSettingS{}
	AppSetting      = &AppSettingS{}
	ServerSetting   = &ServerSettingS{}
	JWTSetting      = &JWTSettingS{}
	WechatSetting   = &WechatSettingS{}
	RedisSetting    = &RedisSettingS{}
	QiniuSetting    = &QiniuSettingS{}
)
