package setting

import (
	"fmt"
	"github.com/silenceper/wechat/v2/cache"
	"github.com/spf13/viper"
	"log"
	"time"
)

type Viper struct {
	vp *viper.Viper
}

func NewViper() (*Viper, error) {
	vp := viper.New()
	vp.SetConfigName("config")
	vp.AddConfigPath("configs/")
	vp.SetConfigType("yaml")
	err := vp.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return &Viper{vp}, nil
}
func (viper *Viper) ReadSection(k string, v interface{}) error {
	err := viper.vp.UnmarshalKey(k, v)
	if err != nil {
		return err
	}
	return nil
}

// Setup 读取config.yaml 初始化setting设置
func Setup() {
	vp, err := NewViper()
	if err != nil {
		log.Fatalf("setting.Setup, fail to init 'Viper': %v", err)
		fmt.Sprintf("setting.Setup, fail to init 'Viper': %v/n", err)
	}
	err = vp.ReadSection("Server", &ServerSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'Server': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'Server': %v/n", err)
	}
	err = vp.ReadSection("App", &AppSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'App': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'App': %v/n", err)
	}
	err = vp.ReadSection("Database", &DatabaseSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'Database': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'Database': %v/n", err)
	}

	//读取JWT 节点配置  初始化  JWTSetting
	err = vp.ReadSection("JWT", &JWTSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'JWT': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'JWT': %v/n", err)
	}
	//读取 Wechat 配置 初始化 WechatSetting
	err = vp.ReadSection("Wechat", &WechatSetting.MiniConfig)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'Wechat': %v", err)
	}
	//读取 WechatRedis 配置 初始化 WechatSetting
	err = vp.ReadSection("WechatRedis ", &WechatSetting.RedisConfig)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'WechatRedis': %v", err)
	}

	//读取 WechatRedis 配置 初始化 WechatCacheType
	err = vp.ReadSection("WechatCacheType", &WechatSetting.CacheTypeConfig)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'WechatRedis': %v", err)
	}

	ServerSetting.ReadTimeout *= time.Second
	ServerSetting.WriteTimeout *= time.Second

	var minaCache cache.Cache
	switch WechatSetting.CacheTypeConfig.CacheType {
	case "memory":
		minaCache = cache.NewMemory()
	case "redis":
		minaCache = cache.NewRedis(&WechatSetting.RedisConfig)
	}
	WechatSetting.MiniConfig.Cache = minaCache

	/*
		redis 配置读取
	*/
	//读取 WechatRedis 配置 初始化 WechatCacheType
	err = vp.ReadSection("Redis", &RedisSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'RedisSetting': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'RedisSetting': %v/n", err)
	}

	/*
		七牛云配置读取
	*/
	err = vp.ReadSection("Qiniu", &QiniuSetting)
	if err != nil {
		log.Fatalf("setting.Setup, fail to readSection 'QiniuSetting': %v", err)
		fmt.Sprintf("setting.Setup, fail to readSection 'QiniuSetting': %v/n", err)
	}
}
