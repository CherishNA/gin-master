package rdb

import (
	"context"
	"github.com/go-redis/redis/v8"
	"log"
	"yxService/app/pkg/setting"
)

var RedisClient *redis.Client
var Ctx = context.Background()

func Setup() {

	rdb := redis.NewClient(&redis.Options{
		Addr:     setting.RedisSetting.Addr,
		Password: setting.RedisSetting.Password,
		DB:       setting.RedisSetting.DB,
	})
	_, err := rdb.Ping(Ctx).Result()
	if err != nil {
		log.Fatalf("init redis err:%s", err.Error())
	}
	RedisClient = rdb
}
