package wechat

import (
	"github.com/silenceper/wechat/v2"
	"github.com/silenceper/wechat/v2/miniprogram"
	"yxService/app/pkg/setting"
)

var Wechat *wechat.Wechat
var Mina *miniprogram.MiniProgram

func Setup() {

	Wechat = wechat.NewWechat()
	Mina = Wechat.GetMiniProgram(&setting.WechatSetting.MiniConfig)
}
