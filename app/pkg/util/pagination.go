package util

import (
	"github.com/gin-gonic/gin"
	"yxService/app/pkg/convert"
	"yxService/app/pkg/setting"
)

func GetPage(c *gin.Context) int {
	page := convert.StrTo(c.Query("page")).MustInt()
	if page <= 0 {
		return 1
	}
	return page
}

func GetPageSize(c *gin.Context) int {
	pageSize := convert.StrTo(c.Query("page_size")).MustInt()
	if pageSize < 0 {
		return setting.AppSetting.DefaultPageSize
	}
	if pageSize > setting.AppSetting.MaxPageSize {
		return setting.AppSetting.DefaultPageSize
	}
	return pageSize
}
func GetPageOffset(page, pageSize int) int {
	result := 0
	if page > 0 {
		result = (page - 1) * pageSize
	}
	return result
}
