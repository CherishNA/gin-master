package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yxService/app/models"
	"yxService/app/pkg/logger"
	"yxService/app/pkg/rdb"
	"yxService/app/pkg/setting"
	"yxService/app/pkg/wechat"
	"yxService/routers"
)

func init() {
	//初始化配置
	setting.Setup()
	logger.SetupLogger()
	models.SetupDBEngine()
	wechat.Setup()
	rdb.Setup()
}

//gin -p 8080 -a 8999 -b gin-bin.exe  --all run
func main() {
	logger.Log.Infof("Design by:%s,Project:%s", "ning", "yxServe.")
	gin.SetMode(setting.ServerSetting.RunMode)
	router := routers.NewRouter()
	s := &http.Server{
		Addr:           ":" + setting.ServerSetting.HttpPort,
		Handler:        router,
		ReadTimeout:    setting.ServerSetting.ReadTimeout,
		WriteTimeout:   setting.ServerSetting.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}
	s.ListenAndServe()
}
