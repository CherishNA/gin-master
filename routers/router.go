package routers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"yxService/app/controller"
	"yxService/app/middleware"
	"yxService/app/pkg/app"
	"yxService/app/pkg/errcode"
	"yxService/app/pkg/logger"
)

func NewRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleware.Cors()) //开启跨域
	r.GET("/", func(context *gin.Context) {
		logger.Log.Info("tools.BindAndValid errs: %v", "NING")
		app.NewResponse(context).ToResponse("星际之猫，一直摸你肚子", errcode.Success)
	})
	noauth := r.Group("/api/")
	{
		noauth.GET("/testGet", func(c *gin.Context) {
			c.JSON(200, gin.H{"msg": "test success"})
		})
		noauth.POST("/login", controller.User{}.CreateUser)
		noauth.POST("/wxAuth", controller.Wechat{}.Auth)
		noauth.POST("/wxLogin", controller.Wechat{}.Login)

	}
	auth := r.Group("/api/auth/").Use(middleware.JWT(), middleware.Cors())
	{
		auth.GET("/test", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{
				"data": "test",
			})
		})
		auth.GET("userInfo", controller.User{}.GetUser)
	}
	return r
}
